//
//  RealmManager.swift
//  myPills
//
//  Created by Alesya Nemkova on 9.12.21.
//

import Foundation
import RealmSwift
import UniformTypeIdentifiers

class RealmManager {
    static let shared = RealmManager()
    private init() {}
    
    private let realm = try! Realm()

    func makeDone(_ entry: MedicationEntry, takeTimeAt: Date) {
        try! realm.write {
            self.setCompletedValue(true, entry: entry, takeTimeAt: takeTimeAt)
        }
    }
    
    func makeUndone(_ entry: MedicationEntry, takeTimeAt: Date) {
        try! realm.write {
            self.setCompletedValue(false, entry: entry, takeTimeAt: takeTimeAt)
        }
    }
    
    private func setCompletedValue(_ value: Bool, entry: MedicationEntry, takeTimeAt: Date) {
        guard let findedObject = Array(realm.objects(MedicationRealmObject.self)).filter({ $0.name == entry.name }).first else {
            return
        }
        guard let findedTakeTime = findedObject.reception?.takeTimes.filter({ $0.time == takeTimeAt }).first else {
            return
        }
        findedTakeTime.isComplete = .init(value: value)
    }
    
    func getEntries() -> [MedicationEntry] {
        
        let entriesRealm = Array(realm.objects(MedicationRealmObject.self))

        var entriesArray = [MedicationEntry]()
        
        for object in entriesRealm {
            let reception: MedicationEntry.Reception
            if let receptionObject = object.reception {
                reception = .init(receptionObject: receptionObject)
            } else {
                reception = MedicationEntry.Reception()
            }
            let myEntryMediation = MedicationEntry(
                name: object.name,
                type: TypeOfMedication(rawValue: object.type)!,
                strengh: object.strengh,
                kind: KindOfTypes(rawValue: object.kind)!,
                color: UIColor(hexString: object.color),
                reception: reception)
            
            entriesArray.append(myEntryMediation)
        }
        return entriesArray
    }

    func writeEntry(_ entry: MedicationEntry) {
        let entryRealm = MedicationRealmObject(entry: entry)
        try! realm.write {
            realm.add(entryRealm)
        }
    }
    
    func deleteEntry(_ entry: MedicationEntry) {
        guard let findedObject = Array(realm.objects(MedicationRealmObject.self)).filter({ $0.name == entry.name }).first else {
            return
        }
        try! realm.write {
            realm.delete(findedObject)
        }
    }
}
