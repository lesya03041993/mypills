//
//  PushManager.swift
//  myPills
//
//  Created by Alesya Nemkova on 19.01.22.
//

import Foundation
import UserNotifications
import UIKit

class PushManager {
    static let shared = PushManager()
    
    func requestAuthorization(completion: @escaping (Bool) -> ()) {
       
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { success, error in
            completion(success)
        }
    }
    
    func scheduleNotifications(startDate: Date, endDate: Date, times: [Date], medicationName: String) {
        
        func schedule() {
            let datesBetween = Date.dates(from: startDate, to: endDate)
            for date in datesBetween {
                for time in times {
                    var components = DateComponents()
                    
                    let day = date.component(.day)
                    let month = date.component(.month)
                    let year = date.component(.year)
                    let hour = time.component(.hour)
                    let minute = time.component(.minute)
                    
                    components.day = day
                    components.month = month
                    components.year = year
                    components.hour = hour
                    components.minute = minute
                    
                    
                    let id = "\(day)-\(month)-\(year)-\(hour)-\(minute)-\(medicationName)"
                    
                    let trigger = createNotificationTrigger(components: components)
                    let content = createNotificationContent(title: "Пора принимать ваши лекарства!", subtitle: "Выпить \(medicationName)")
                    addNotification(trigger: trigger, content: content, identifier: id)
                }
            }
        }
        
        requestAuthorization { success in
            if success {
                schedule()
            }
        }
    }
    
    private func createNotificationContent(title: String, subtitle: String) -> UNMutableNotificationContent  {
        let content = UNMutableNotificationContent()
        content.title = title
        content.subtitle = subtitle
        content.sound = .default
        return content
    }
    
    private func createNotificationTrigger(components: DateComponents) -> UNNotificationTrigger {
        return UNCalendarNotificationTrigger(dateMatching: components, repeats: false)
    }
    
    private func addNotification(trigger: UNNotificationTrigger, content: UNNotificationContent, identifier: String) {
        let request = UNNotificationRequest(identifier: identifier,
                                            content: content,
                                            trigger: trigger)
        
        UNUserNotificationCenter.current().add(request)
    }
}

