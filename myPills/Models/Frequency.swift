//
//  Frequency.swift
//  myPills
//
//  Created by Alesya Nemkova on 13.01.22.
//

import Foundation

enum Frequency {
    case everyday
    case specific(days: [DayOfWeek])
    
    var name: String {
        switch self {
        case .everyday: return "Ежедневно"
        case .specific(_): return "Определенные дни недели"
        }
    }
    
    var days: [Int] {
        switch self {
        case .everyday: return DayOfWeek.allCases.map { $0.rawValue }
        case .specific(let days): return days.map { $0.rawValue }
        }
    }
}

enum DayOfWeek: Int, CaseIterable {
    case mon
    case tue
    case wed
    case thu
    case fri
    case sat
    case sun
    
    var name : String {
        switch self {
        case .mon: return "ПН"
        case .tue: return "ВТ"
        case .wed: return "СР"
        case .thu: return "ЧТ"
        case .fri: return "ПТ"
        case .sat: return "СБ"
        case .sun: return "ВС"
        }
    }
}

