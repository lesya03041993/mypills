//
//  KindOfTypes.swift
//  myPills
//
//  Created by Alesya Nemkova on 11.01.22.
//

import Foundation
import UIKit

enum KindOfTypes: String {
    case mg
    case g
    case ml
    case mcg
    case ME
    case mgG
    case mgMl
    case thing
    
    var name: String {
        switch self {
        case .mg: return "мг"
        case .g: return "гр"
        case .mcg: return "мкг"
        case .ml: return "мл"
        case .ME: return "ME"
        case .mgG: return "мг-г"
        case .mgMl: return "мг-мл"
        case .thing: return "штука"
        }
    }
}

