//
//  MedicationRealmObject.swift
//  myPills
//
//  Created by Alesya Nemkova on 13.01.22.
//

import Foundation
import RealmSwift

class TakeTimeCompletedObject: Object {
    @objc dynamic var value = false

    convenience init(value: Bool) {
        self.init()
        self.value = value
    }
}

class TakeTimeObject: Object {
    @objc dynamic var count: Double = 0.0
    @objc dynamic var time = Date()
    @objc dynamic var isComplete: TakeTimeCompletedObject? = nil
    
    convenience init(count: Double,
                     time: Date,
                     isComplete: TakeTimeCompletedObject?) {
        self.init()
        self.count = count
        self.time = time
        self.isComplete = isComplete
    }
}

class ReceptionRealmObject: Object {
    let takeTimes = List<TakeTimeObject>()
    let days = List<Int>()
    @objc dynamic var startDate = Date()
    @objc dynamic var endDate = Date()
    
    convenience init(days: [Int],
                     startDate: Date,
                     endDate: Date,
                     takeTimes: [MedicationEntry.Reception.TakeTime]
    ) {
        self.init()
        self.days.append(objectsIn: days)
        self.startDate = startDate
        self.endDate = endDate
        for model in takeTimes {
            var completeObject: TakeTimeCompletedObject? = nil
            if let isComplete = model.isComplete {
                completeObject = TakeTimeCompletedObject(value: isComplete)
            }
            let object = TakeTimeObject(count: model.count, time: model.time, isComplete: completeObject)
            self.takeTimes.append(object)
        }
    }
}

class MedicationRealmObject: Object {
    
    @objc dynamic var asd: Bool = false
    @objc dynamic var name: String = ""
    @objc dynamic var type: String = ""
    @objc dynamic var strengh: String = ""
    @objc dynamic var kind: String = ""
    @objc dynamic var color: String = ""
    @objc dynamic var reception: ReceptionRealmObject? = nil
    
//    override class func primaryKey() -> String? {
//        return self.name
//    }
    convenience init(name: String,
                     type: String,
                     strengh: String,
                     kind: String,
                     color: String,
                     reception: ReceptionRealmObject?) {
        self.init()
        self.name = name
        self.type = type
        self.strengh = strengh
        self.kind = kind
        self.color = color
        self.reception = reception
        
    }
}

// write to the realm
extension MedicationRealmObject {
    
    convenience init(entry: MedicationEntry) {
        self.init(
            name: entry.name,
            type: entry.type.rawValue,
            strengh: entry.strengh,
            kind: entry.kind.rawValue,
            color: entry.color.hexCode,
            reception: .init(
                days: entry.reception.frequency.days,
                startDate: entry.reception.startDate,
                endDate: entry.reception.endDate,
                takeTimes: entry.reception.takeTimes
            )
        )
    }
}
