//
//  MedicationEntry.swift
//  myPills
//
//  Created by Alesya Nemkova on 12.01.22.
//

import Foundation
import RealmSwift

struct MedicationEntry {
    struct Reception {
        struct TakeTime {
            var count: Double
            var time: Date
            var isComplete: Bool?
            init(count: Double,
                 time: Date,
                 isComplete: Bool?
            ) {
                self.count = count
                self.time = time
                self.isComplete = isComplete
            }
        }
        var frequency: Frequency
        var startDate: Date
        var endDate: Date
        var takeTimes: [TakeTime]
    }
    
    var name: String
    var type: TypeOfMedication
    var strengh: String
    var kind: KindOfTypes
    var color: UIColor
    var reception: Reception

    init(name: String,
         type: TypeOfMedication,
         strengh: String,
         kind: KindOfTypes,
         color: UIColor,
         reception: Reception
    ) {
        self.name = name
        self.type = type
        self.strengh = strengh
        self.kind = kind
        self.color = color
        self.reception = reception
    }
}

// got it from the realm
extension MedicationEntry {
    init(name: String,
         type: String,
         strengh: String,
         kind: String,
         color: String,
         reception: ReceptionRealmObject
    ) {
        self.name = name
        
        guard let type = TypeOfMedication(rawValue: type) else {
            fatalError("no medication type")
        }
        
        self.type = type
        self.strengh = strengh
        
        guard let kind = KindOfTypes(rawValue: kind) else {
            fatalError("no medication kind")
        }
        
        self.kind = kind
        self.color = UIColor(hexString: color)
        self.reception = MedicationEntry.Reception(receptionObject: reception)
    }
}

extension MedicationEntry.Reception {
    init() {
        self.frequency = .everyday
        self.startDate = Date()
        self.endDate = Date()
        self.takeTimes = []
    }
    
    init(receptionObject: ReceptionRealmObject) {
        if receptionObject.days.count == 7 {
            self.frequency = .everyday
        } else {
            var daysOutput: [DayOfWeek] = []
            for dayInt in receptionObject.days {
                if let day = DayOfWeek(rawValue: dayInt) {
                    daysOutput.append(day)
                }
            }
            self.frequency = .specific(days: daysOutput)
        }
        self.startDate = receptionObject.startDate
        self.endDate = receptionObject.endDate
        var takeTimes = [MedicationEntry.Reception.TakeTime]()
        for object in receptionObject.takeTimes {
            takeTimes.append(MedicationEntry.Reception.TakeTime(count: object.count, time: object.time, isComplete: object.isComplete?.value))
        }
        self.takeTimes = takeTimes
    }
}
