//
//  TypeOfMedication.swift
//  myPills
//
//  Created by Alesya Nemkova on 9.01.22.
//

import Foundation
import UIKit

enum TypeOfMedication: String {
    case pill
    case capsul
    case cream
    case spray
    case injection
    case drops
    case solution
    case procedure
    
    var name: String {
        switch self {
        case .pill: return "Таблетки"
        case .capsul: return "Капсулы"
        case .cream: return "Мазь"
        case .spray: return "Спрей"
        case .injection: return "Уколы"
        case .drops: return "Капли"
        case .solution: return "Жидкость"
        case .procedure: return "Процедуры"
        }
    }
    
    var image: UIImage? {
        switch self {
        case .pill: return UIImage(named: "pill")
        case .capsul: return UIImage(named: "capsul")
        case .cream: return UIImage(named: "cream")
        case .spray: return UIImage(named: "spray")
        case .injection: return UIImage(named: "injection")
        case .drops: return UIImage(named: "drops")
        case .solution: return UIImage(named: "solution")
        case .procedure: return UIImage(named: "procedure")
        }
    }
}
