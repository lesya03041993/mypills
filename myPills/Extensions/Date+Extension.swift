//
//  Date+Extension.swift
//  myPills
//
//  Created by Alesya Nemkova on 18.01.22.
//

import Foundation
import UIKit

extension Date {

    static func makeDate(hr: Int, min: Int) -> Date {
        let calendar = Calendar(identifier: .gregorian)
        let components = DateComponents(hour: hr, minute: min)
        return calendar.date(from: components)!
    }
}

extension Date {
    static func dates(from fromDate: Date, to toDate: Date) -> [Date] {
        var dates: [Date] = []
        var date = fromDate
        
        while date <= toDate {
            dates.append(date)
            guard let newDate = Calendar.current.date(byAdding: .day, value: 1, to: date) else { break }
            date = newDate
        }
        return dates
    }
}

extension Date {
    func component(_ component: Calendar.Component) -> Int {
        Calendar.current.component(component, from: self)
    }
}
