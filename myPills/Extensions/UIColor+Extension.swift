//
//  UIColor+Extension.swift
//  myPills
//
//  Created by Alesya Nemkova on 19.12.21.
//

import Foundation
import UIKit

extension UIColor {
    open class var buttonColor: UIColor {
        return UIColor(named: "ButtonColor")!
    }
    
    open class var buttonBigColor: UIColor {
        return UIColor(named: "ButtonBigColor")!
    }
    
    open class var tabBarColor: UIColor {
        return UIColor(named: "TabBarColor")!
    }
    
    open class var mainAppColor: UIColor {
        return UIColor(named: "MainAppColor")!
    }
    
    open class var tint: UIColor {
        return UIColor(named: "Tint")!
    }
    
    open class var green: UIColor {
        return UIColor(named: "Green")!
    }
    
    open class var red: UIColor {
        return UIColor(named: "Red")!
    }
    
    open class var blueForType: UIColor {
        return UIColor(named: "BlueForType")!
    }
    
    open class var pinkForType: UIColor {
        return UIColor(named: "PinkForType")!
    }
    
    open class var redForType: UIColor {
        return UIColor(named: "RedForType")!
    }
    
    open class var greenForType: UIColor {
        return UIColor(named: "GreenForType")!
    }
    
    open class var yellowForType: UIColor {
        return UIColor(named: "YellowForType")!
    }
}

extension UIColor {
    var hexCode: String {
        get{
            let colorComponents = self.cgColor.components!
            if colorComponents.count < 4 {
                return String(format: "%02x%02x%02x", Int(colorComponents[0]*255.0), Int(colorComponents[0]*255.0),Int(colorComponents[0]*255.0)).uppercased()
            }
            return String(format: "%02x%02x%02x", Int(colorComponents[0]*255.0), Int(colorComponents[1]*255.0),Int(colorComponents[2]*255.0)).uppercased()
        }
    }
    
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        var hexInt: UInt32 = 0
        let scanner: Scanner = Scanner(string: hexString)
        scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
        hexInt = UInt32(bitPattern: scanner.scanInt32(representation: .hexadecimal) ?? 0)
        
        let hexint = Int(hexInt)
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
}

