//
//  File.swift
//  myPills
//
//  Created by Alesya Nemkova on 20.12.21.
//

import Foundation
import UIKit

extension UIViewController {
   
   func add(_ controller: UIViewController) {
      addChild(controller)
      view.addSubview(controller.view)
      
      controller.view.translatesAutoresizingMaskIntoConstraints = false
      NSLayoutConstraint.activate([
         controller.view.leadingAnchor.constraint(equalTo: view.leadingAnchor),
         controller.view.topAnchor.constraint(equalTo: view.topAnchor),
         controller.view.trailingAnchor.constraint(equalTo: view.trailingAnchor),
         controller.view.bottomAnchor.constraint(equalTo: view.bottomAnchor),
      ])
      
      controller.didMove(toParent: self)
   }
   
   func remove() {
      guard parent != nil else {
         return
      }

      willMove(toParent: nil)
      view.removeFromSuperview()
      removeFromParent()
   }
}
