//
//  UIFont+Extension.swift
//  myPills
//
//  Created by Alesya Nemkova on 5.01.22.
//

import Foundation
import UIKit

extension UIFont {
    
    static func heavyFont(with size: CGFloat) -> UIFont {
        return UIFont(name: "Nexa-Trial-Heavy", size: size)!
    }
    
    static func regularFont(with size: CGFloat) -> UIFont {
        return UIFont(name: "Nexa-Trial-Regular", size: size)!
    }
    
    static func boldFont(with size: CGFloat) -> UIFont {
        return UIFont(name: "Nexa-Trial-Bold", size: size)!
    }
}
