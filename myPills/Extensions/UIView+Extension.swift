//
//  UIView+Extension.swift
//  myPills
//
//  Created by Alesya Nemkova on 17.01.22.
//

import Foundation
import UIKit

extension UIView {
    
    func shadowOfView(view: UIView) {
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 10
        view.layer.shadowOffset = CGSize(width: 0.5, height: 1)
        view.layer.shadowRadius = 2
        view.layer.shadowColor = UIColor.darkGray.cgColor
        view.layer.shadowOpacity = 0.2
    }    
}
