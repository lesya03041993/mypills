//
//  DaysOfWeekCollectionableCell.swift
//  myPills
//
//  Created by Alesya Nemkova on 15.01.22.
//

import UIKit

class DaysOfWeekCollectionableCell: UICollectionViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var placeholerView: UIView!
    
    private var kindOfType: [DayOfWeek] = [.mon, .tue, .wed, .thu, .fri, .sat, .sun]
    
    private let sectionInsets = UIEdgeInsets(top: 10, left: 20, bottom: 20, right: 20)
    
    private var currentKindArray: [DayOfWeek] = [] {
        didSet {
            daysChanged?(currentKindArray)
        }
    }
    
    var daysChanged: (([DayOfWeek]) -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViewCollection()
        collectionView.backgroundColor = .mainAppColor
        placeholerView.isHidden = true
    }
    
    private func setupViewCollection() {
        let kindCell = UINib(nibName: String(describing: KindCell.self), bundle: nil)
        collectionView.register(kindCell, forCellWithReuseIdentifier: String(describing: KindCell.self))
        collectionView.showsHorizontalScrollIndicator = false
    }
    
    func configureCell(isEnabled: Bool, selectedDays: [DayOfWeek]) {
        placeholerView.isHidden = isEnabled
        currentKindArray = selectedDays
        collectionView.reloadData()
    }
}

extension DaysOfWeekCollectionableCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        kindOfType.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: KindCell.self), for: indexPath)
        guard let kindCell = cell as? KindCell else { return cell }
        let currentKind = kindOfType[indexPath.row]
        let name = currentKind.name
        let isSelected = self.currentKindArray.contains(currentKind)
        kindCell.configure(name: name, isSelected: isSelected)
        
        return kindCell
    }
    
}

extension DaysOfWeekCollectionableCell {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let currentKind = kindOfType[indexPath.row]
        if currentKindArray.contains(currentKind),
           let indexToRemove = currentKindArray.firstIndex(where: { $0 == currentKind }) {
            currentKindArray.remove(at: indexToRemove)
        } else {
            self.currentKindArray.append(currentKind)
        }
        self.collectionView.reloadData()
        
    }
}

extension DaysOfWeekCollectionableCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = 65 - sectionInsets.top - sectionInsets.bottom
        return CGSize(width: 75, height: height)
    }
}


extension DaysOfWeekCollectionableCell {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
}
