//
//  AddTimeCell.swift
//  myPills
//
//  Created by Alesya Nemkova on 15.01.22.
//

import UIKit

class AddTimeCell: UICollectionViewCell {
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var stepper: UIStepper!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    
    var countChanged: ((Double) -> ())?
    var timeChanged: ((Date) -> ())?
    var removeClick: (() -> ())?
        
    override func awakeFromNib() {
        setup()
    }
    
    private func setup() {
        shadowOfView(view: containerView)
        setupStepper()
        setupLabel()
        setupPicker()
        setupRemoveButton()
    }
    
    private func setupRemoveButton() {
        removeButton.addTarget(self, action: #selector(removeButtonAction), for: .touchUpInside)
    }
    
    private func setupPicker() {
        datePicker.minuteInterval = 1
        datePicker.addTarget(self, action: #selector(timeChangedAction), for: .valueChanged)
    }
    
    private func setupStepper() {
        stepper.addTarget(self, action: #selector(stepperChanged), for: .valueChanged)
        stepper.minimumValue = 0.5
        stepper.value = 0.5
        stepper.stepValue = 0.5
    }
    
    private func setupLabel() {
        label.text = "\(stepper.value) шт."
    }
    
}

// MARK: - Actions

extension AddTimeCell {
    
    @objc private func timeChangedAction() {
        timeChanged?(datePicker.date)
    }
    
    @objc private func stepperChanged() {
        setupLabel()
        countChanged?(stepper.value)
    }
    
    @objc private func removeButtonAction() {
        removeClick?()
    }
}

