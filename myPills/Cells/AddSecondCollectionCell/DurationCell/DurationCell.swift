//
//  DurationCell.swift
//  myPills
//
//  Created by Alesya Nemkova on 15.01.22.
//

import UIKit

class DurationCell: UICollectionViewCell {
    @IBOutlet weak var startDayPicker: UIDatePicker!
    @IBOutlet weak var endDatePicker: UIDatePicker!
    
    let formatter = DateFormatter()
    
    var startDateChanged: ((Date) -> ())?
    var endDateChanged: ((Date) -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    private func setup() {
        setupDateFormatter()
        setupStartDate()
        setupEndDate()
    }
    
    private func setupDateFormatter() {
        formatter.dateFormat = "dd-MM-yyyy"
    }
    
    private func setupStartDate() {
        startDayPicker.addTarget(self, action: #selector(dateStartChangedAction), for: .valueChanged)
    }
    
    private func setupEndDate() {
        endDatePicker.addTarget(self, action: #selector(dateEndChangedAction), for: .valueChanged)
    }
}

extension DurationCell {
    
    @objc private func dateStartChangedAction() {
        startDateChanged?(startDayPicker.date)
    }
    
    @objc private func dateEndChangedAction() {
        endDateChanged?(endDatePicker.date)
    }
}
