//
//  AddTimeButtonCell.swift
//  myPills
//
//  Created by Alesya Nemkova on 15.01.22.
//

import UIKit

class AddTimeButtonCell: UICollectionViewCell {

    @IBOutlet weak var button: UIButton!

    var click: (() -> ())?
    override func awakeFromNib() {
        super.awakeFromNib()
        
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
    }
    
    @objc private func buttonAction() {
        click?()
    }
}
