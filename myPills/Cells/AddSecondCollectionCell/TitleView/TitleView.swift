//
//  TitleView.swift
//  myPills
//
//  Created by Alesya Nemkova on 16.01.22.
//

import UIKit

class TitleView: UICollectionReusableView {
    @IBOutlet weak var titleLabel: UILabel!
    
    func configureView(name: String) {
        titleLabel.text = name
    }
}

