//
//  FrequencyCell.swift
//  myPills
//
//  Created by Alesya Nemkova on 12.01.22.
//

import UIKit

class FrequencyCell: UICollectionViewCell {
    @IBOutlet weak var everydayView: UIView!
    @IBOutlet weak var dayView: UIView!
    @IBOutlet weak var everydayLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var daySelectedView: UIImageView!
    @IBOutlet weak var everydaySelectedView: UIImageView!
    
    enum FrequencyType {
        case everyday
        case specific
        var title: String {
            switch self {
            case .everyday: return "Ежедневно"
            case .specific: return "Определенные дни недели"
            }
        }
    }
    var typeChanged: ((FrequencyType) -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        hiddenSelectedView()
        
        let tapEveryday = UITapGestureRecognizer(target: self, action: #selector(tapChageEveryday))
        everydayView.addGestureRecognizer(tapEveryday)
        
        let tapDay = UITapGestureRecognizer(target: self, action: #selector(tapChageDay))
        dayView.addGestureRecognizer(tapDay)
    }
    
    func setupUI() {
        let shadowsView: [UIView] = [everydayView, dayView]
        
        for item in shadowsView {
            shadowOfView(view: item)
        }
        
        everydayLabel.font = UIFont.heavyFont(with: 14)
        dayLabel.font = UIFont.heavyFont(with: 14)
        
        everydayLabel.text = FrequencyType.everyday.title
        dayLabel.text = FrequencyType.specific.title
    }
    
    @objc func tapChageEveryday() {
        selectView(type: .everyday)
        typeChanged?(.everyday)
    }
    
    @objc func tapChageDay() {
        selectView(type: .specific)
        typeChanged?(.specific)
    }
    
    func hiddenSelectedView() {
        daySelectedView.isHidden = true
        everydaySelectedView.isHidden = true
    }
    
    private func selectView(type: FrequencyType) {
        hiddenSelectedView()
        switch type {
        case .everyday:
            everydaySelectedView.isHidden = false
        case .specific:
            daySelectedView.isHidden = false
        }
    }
    
    func configure(selectedType: FrequencyType) {
        selectView(type: selectedType)
    }
}
