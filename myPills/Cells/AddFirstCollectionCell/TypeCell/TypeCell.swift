//
//  TypeCell.swift
//  myPills
//
//  Created by Alesya Nemkova on 9.01.22.
//

import UIKit

class TypeCell: UICollectionViewCell {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var typeImage: UIImageView!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var selectedVIew: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
     
    }
    
    func setupUI() {
        shadowOfView(view: containerView)
        colorView.layer.cornerRadius = colorView.bounds.height / 2
    }
    
    func configure(name: String, image: UIImage?, isSelected: Bool) {
        typeLabel.text = name
        typeImage.image = image
        if isSelected {
            selectedVIew.isHidden = false
        } else {
            selectedVIew.isHidden = true
        }
    }
}
