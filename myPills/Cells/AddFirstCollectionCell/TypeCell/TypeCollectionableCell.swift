//
//  TypeCollectionableCell.swift
//  myPills
//
//  Created by Alesya Nemkova on 9.01.22.
//

import UIKit


class TypeCollectionableCell: UICollectionViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    
    let sectionInsets = UIEdgeInsets(top: 25, left: 20, bottom: 5, right: 20)
    
    var availableType: [TypeOfMedication] = [.pill, .capsul, .cream, .drops, .injection, .procedure, .spray, .solution]
    
    var currentType = TypeOfMedication.pill {
        didSet {
            typeChanged?(currentType)
        }
    }
    
    var typeChanged: ((TypeOfMedication) -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()

        collectionView.dataSource = self
        collectionView.delegate = self

        setupViewCollection()
        collectionView.backgroundColor = .mainAppColor
    }
    
    private func setupViewCollection() {
        let typeCell = UINib(nibName: String(describing: TypeCell.self), bundle: nil)
        collectionView.register(typeCell, forCellWithReuseIdentifier: String(describing: TypeCell.self))
    }
}

extension TypeCollectionableCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        availableType.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: TypeCell.self), for: indexPath)
        guard let typeCell = cell as? TypeCell else { return cell }
        let currentType = availableType[indexPath.row]
        let name = currentType.name
        let image = currentType.image
        let isSelected = currentType == self.currentType
        typeCell.configure(name: name, image: image, isSelected: isSelected)
        return typeCell
    }
}

extension TypeCollectionableCell: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let currentType = availableType[indexPath.row]
        self.currentType = currentType
        self.collectionView.reloadData()
    }
}

extension TypeCollectionableCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = 160 - sectionInsets.top - sectionInsets.bottom
        return CGSize(width: 100, height: height)
    }
}


extension TypeCollectionableCell {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
}
