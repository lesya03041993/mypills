//
//  KindCollectionableCell.swift
//  myPills
//
//  Created by Alesya Nemkova on 11.01.22.
//

import UIKit

class KindCollectionableCell: UICollectionViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    
    var kindOfType: [KindOfTypes] = [.mg, .g, .mcg, .ME, .ml, .mgMl, .mgG, .thing]
    
    let sectionInsets = UIEdgeInsets(top: 25, left: 20, bottom: 5, right: 20)
    
    var currentKind = KindOfTypes.mg {
        didSet {
            kindChanged?(currentKind)
        }
    }
    
    var kindChanged: ((KindOfTypes) -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViewCollection()
        collectionView.backgroundColor = .mainAppColor
    }
    
    private func setupViewCollection() {
        let kindCell = UINib(nibName: String(describing: KindCell.self), bundle: nil)
        collectionView.register(kindCell, forCellWithReuseIdentifier: String(describing: KindCell.self))
    }
}

extension KindCollectionableCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        kindOfType.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: KindCell.self), for: indexPath)
        guard let kindCell = cell as? KindCell else { return cell }
        let currentKind = kindOfType[indexPath.row]
        let name = currentKind.name
        let isSelected = currentKind == self.currentKind
        kindCell.configure(name: name, isSelected: isSelected)
        
        return kindCell
    }
}

extension KindCollectionableCell {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let currentKind = kindOfType[indexPath.row]
        self.currentKind = currentKind
        self.collectionView.reloadData()
    }
}

extension KindCollectionableCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = 65 - sectionInsets.top - sectionInsets.bottom
        return CGSize(width: 75, height: height)
    }
}

extension KindCollectionableCell {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
}

