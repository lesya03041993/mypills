//
//  KindCell.swift
//  myPills
//
//  Created by Alesya Nemkova on 11.01.22.
//

import UIKit

class KindCell: UICollectionViewCell {
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backView.layer.cornerRadius = 18
    }
    
    func configure(name: String, isSelected: Bool) {
        nameLabel.text = name
        if isSelected  {
            backView.backgroundColor = .buttonColor
        } else {
            backView.backgroundColor = .tabBarColor
        }
    }
}
