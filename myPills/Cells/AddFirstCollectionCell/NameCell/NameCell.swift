//
//  NameCell.swift
//  myPills
//
//  Created by Alesya Nemkova on 8.01.22.
//

import UIKit

class NameCell: UICollectionViewCell {
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var nameLabel: UILabel!
    
    var nameChanged: ((String) -> ())?
 
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        addDoneButtonOnNumpad(textField: nameField)
        
    }
    
    private func setupUI() {
        nameField.layer.cornerRadius = 5
        nameField.layer.borderWidth = 1
        nameField.layer.borderColor = UIColor.tint.cgColor
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 20))
        nameField.leftView = paddingView
        nameField.leftViewMode = .always
        nameField.addTarget(self, action: #selector(textDidChange(sender:)), for: .editingChanged)
    }
    
    @objc private func textDidChange(sender: UITextField) {
        guard let text = nameField.text, !text.isEmpty else { return }
        nameChanged?(text)
    }
    
    func setupTitle(title: String, keyBoardNumber: Bool) {
        nameLabel.text = title
        nameField.keyboardType = keyBoardNumber ? .decimalPad : .asciiCapable
        
    }
    
    func addDoneButtonOnNumpad(textField: UITextField) {
            let keypadToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 40))
        keypadToolbar.items=[
                UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: self, action: nil),
                UIBarButtonItem(title: "Сохранить", style: UIBarButtonItem.Style.done, target: textField, action: #selector(UITextField.resignFirstResponder))
            ]
            keypadToolbar.sizeToFit()
        keypadToolbar.tintColor = .tint
        textField.inputAccessoryView = keypadToolbar
        }
}


