//
//  ColorCell.swift
//  myPills
//
//  Created by Alesya Nemkova on 11.01.22.
//

import UIKit

class ColorCell: UICollectionViewCell {
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var selectedImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    func setupUI() {
        colorView.layer.borderColor = UIColor.systemGray6.cgColor
        colorView.layer.borderWidth = 1
        colorView.layer.cornerRadius = colorView.frame.height / 2
    }
        
    func configure(color: UIColor, isSelected: Bool) {
        colorView.backgroundColor = color
        if isSelected  {
            selectedImage.isHidden = false
        } else {
            selectedImage.isHidden = true
        }
    }
}


