//
//  ColorCollectionableCell.swift
//  myPills
//
//  Created by Alesya Nemkova on 11.01.22.
//

import UIKit

class ColorCollectionableCell: UICollectionViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    
    let sectionInsets = UIEdgeInsets(top: 25, left: 20, bottom: 5, right: 20)
    
    var availableColor: [UIColor] = [.white, .blueForType, .pinkForType, .redForType, .greenForType, .yellowForType]
    
    var selectedColor = UIColor.blueForType {
        didSet {
            colorChanged?(selectedColor)
        }
    }
    
    var colorChanged: ((UIColor) -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViewCollection()
        collectionView.backgroundColor = .mainAppColor
    }
    
    private func setupViewCollection() {
        let colorCell = UINib(nibName: String(describing: ColorCell.self), bundle: nil)
        collectionView.register(colorCell, forCellWithReuseIdentifier: String(describing: ColorCell.self))
    }
}


extension ColorCollectionableCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        availableColor.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ColorCell.self), for: indexPath)
        guard let colorCell = cell as? ColorCell else { return cell }
        let currentColor = availableColor[indexPath.row]
        let isSelected = currentColor == self.selectedColor
        colorCell.configure(color: currentColor, isSelected: isSelected)
        
        return colorCell
    }
}

extension ColorCollectionableCell {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let currentColor = availableColor[indexPath.row]
        self.selectedColor = currentColor
        self.collectionView.reloadData()
    }
}

extension ColorCollectionableCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = 80 - sectionInsets.top - sectionInsets.bottom
        return CGSize(width: 50, height: height)
    }
}

extension ColorCollectionableCell {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
}
