//
//  HeaderCell.swift
//  myPills
//
//  Created by Alesya Nemkova on 18.01.22.
//

import UIKit

class HeaderCell: UITableViewCell {
    @IBOutlet weak var headerLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(time: String) {
        headerLabel.text = time
    }
}
