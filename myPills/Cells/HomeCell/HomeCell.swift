//
//  HomeCell.swift
//  myPills
//
//  Created by Alesya Nemkova on 26.12.21.
//

import UIKit

class HomeCell: UITableViewCell {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var nameOfMedicineLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var strengthLabel: UILabel!
    @IBOutlet weak var typeImage: UIImageView!
    @IBOutlet weak var isCompletedImageView: UIImageView!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var missLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       setupUI()
    }
    
    func configure(entry: MedicationEntry, takeTime: Date) {
        typeImage.image = entry.type.image
        typeLabel.text = entry.type.name
        nameOfMedicineLabel.text = entry.name
        strengthLabel.text = ("\(entry.strengh) \(entry.kind)")
        if let takeTimeExactly = entry.reception.takeTimes.first(where: { $0.time == takeTime }) {
            if let isComplete = takeTimeExactly.isComplete {
                missLabel.isHidden = false
                isCompletedImageView.isHidden = false
                if isComplete {
                    isCompletedImageView.image = UIImage(systemName: "checkmark.circle.fill")
                    isCompletedImageView.tintColor = UIColor.green
                    missLabel.text = "Принято"
                    missLabel.textColor = UIColor.green
                
                } else {
                    isCompletedImageView.image = UIImage(systemName: "exclamationmark.circle.fill")
                    isCompletedImageView.tintColor = UIColor.red
                    missLabel.text = "Пропущено"
                    missLabel.textColor = UIColor.red
                }
            } else {
                isCompletedImageView.isHidden = true
                missLabel.isHidden = true
            }
            countLabel.text = "\(takeTimeExactly.count)"
            
        }
        colorView.backgroundColor = entry.color.withAlphaComponent(0.2)
    }
    
    func setupUI() {
        colorView.layer.masksToBounds = false
        colorView.layer.cornerRadius = 10
        shadowOfView(view: containerView)
    }
}
    
   

