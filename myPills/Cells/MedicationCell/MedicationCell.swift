//
//  MedicationCell.swift
//  myPills
//
//  Created by Alesya Nemkova on 6.01.22.
//

import UIKit

class MedicationCell: UITableViewCell {
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var typeImage: UIImageView!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var nameOfMedicineLabel: UILabel!
    @IBOutlet weak var strengthLabel: UILabel!
    @IBOutlet weak var frequencyLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    let dateFormatter = DateFormatter()
    
    override func awakeFromNib() {
        super.awakeFromNib()
       setupUI()
        
        dateFormatter.dateFormat = "HH:mm"
    }
    
    func configure(entry: MedicationEntry) {
        typeImage.image = entry.type.image
        typeLabel.text = entry.type.name
        nameOfMedicineLabel.text = entry.name
        strengthLabel.text = ("\(entry.strengh) \(entry.kind)")
        frequencyLabel.text = "\(entry.reception.frequency.name) - "
        
        var timeString = ""
        
        var counter = 0
        for itemTime in entry.reception.takeTimes {
            let strTime = dateFormatter.string(from: itemTime.time)
            
            timeString.append(strTime)
            
            if counter != entry.reception.takeTimes.count - 1 {
                timeString.append(", ")
            }
            counter += 1
        }
        
        timeLabel.text = timeString
        colorView.backgroundColor = entry.color.withAlphaComponent(0.2)
    }
    
    func setupUI() {
        colorView.layer.masksToBounds = false
        colorView.layer.cornerRadius = 10
        shadowOfView(view: containerView)
    }
}
