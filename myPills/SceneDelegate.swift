//
//  SceneDelegate.swift
//  myPills
//
//  Created by Alesya Nemkova on 9.12.21.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    var tabBarController: UITabBarController?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        configureNavBar()
        window = UIWindow(windowScene: windowScene)
        window?.windowScene = windowScene
        let tabBarController = creatTabBar()
        self.tabBarController = tabBarController
        window?.rootViewController = tabBarController
        
        window?.makeKeyAndVisible()
    }
    
    func creatTabBar() -> UITabBarController {
        let tabBarController = UITabBarController()

        let firstIteam = UITabBarItem(title: "Главная", image: UIImage(systemName: "house"), tag: 0)
        let homeVC = HomeContainerController()
        homeVC.tabBarItem = firstIteam
        
//        let secondItem = UITabBarItem(title: "Календарь", image: UIImage(systemName: "calendar"), tag: 1)
//        let calendarVC = CalendarController(nibName: String(describing: CalendarController.self), bundle: nil)
//        calendarVC.tabBarItem = secondItem
        
        let thirdItem = UITabBarItem(title: "Лекарства", image: UIImage(systemName: "pills" ), tag: 2)
        let medicationsVC = MedicationsContainerController()
        medicationsVC.tabBarItem = thirdItem
        
        let controllers: [UINavigationController] = [UINavigationController(rootViewController: homeVC),
//                                                     UINavigationController(rootViewController: calendarVC),
                                                     UINavigationController(rootViewController: medicationsVC)]
        
        tabBarController.viewControllers = controllers
        tabBarController.tabBar.backgroundColor = .tabBarColor
        tabBarController.tabBar.tintColor = .tint
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont.boldFont(with: 12)], for: .normal)
        return tabBarController
    }
    
    func configureNavBar() {
        if #available(iOS 15, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            UINavigationBar.appearance().standardAppearance = appearance
            UINavigationBar.appearance().scrollEdgeAppearance = appearance
        }
    }
}

