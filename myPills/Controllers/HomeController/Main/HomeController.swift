//
//  HomeController.swift
//  myPills
//
//  Created by Alesya Nemkova on 18.12.21.
//

import UIKit

class HomeController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
        
    let timeFormatter = DateFormatter()
        
    struct Section {
        let date: Date
        var entries: [MedicationEntry]
    }
    
    var sections: [Section] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        
        timeFormatter.dateFormat = "HH:mm"
        configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        loadData()
    }
    
    private func configureUI() {
        tableView.backgroundColor = .mainAppColor
        tableView.separatorStyle = .none
        
        let homeNib = UINib(nibName: String(describing: HomeCell.self), bundle: nil)
        tableView.register(homeNib, forCellReuseIdentifier: String(describing: HomeCell.self))
        
        let headerNib = UINib(nibName: String(describing: HeaderCell.self), bundle: nil)
        tableView.register(headerNib, forCellReuseIdentifier: String(describing: HeaderCell.self))
        
    }
    
    private func loadData() {
        let medicationEntery = RealmManager.shared.getEntries()

        var sections: [Section] = []
        
        for entry in medicationEntery {
            guard nowDateIsBetween(date1: entry.reception.startDate, date2: entry.reception.endDate) else {
                continue
            }

            for time in entry.reception.takeTimes {
                if let sectionIndex = sections.firstIndex(where: { self.compareTwoTimes(date1: $0.date, date2: time.time, formatter: self.timeFormatter) }) {
                    var section = sections[sectionIndex]
                    
                    guard section.entries.contains(where: { $0.name != entry.name}) else {
                        continue
                    }
                    section.entries.append(entry)
                    
                    sections[sectionIndex] = section
                } else {
                    sections.append(Section(date: time.time, entries: [entry]))
                }
            }
        }
        
        // Sort by time
        self.sections = sections.sorted(by: { section1, section2 in
            let date1 = section1.date
            let date2 = section2.date
            
            let components1 = Calendar.current.dateComponents([.hour, .minute], from: date1)
            let components2 = Calendar.current.dateComponents([.hour, .minute], from: date2)
            
            guard let hour1 = components1.hour, let minute1 = components1.minute,
                  let hour2 = components2.hour, let minute2 = components2.minute
            else {
                return false
            }
            
            if hour1 < hour2 {
                return true
            } else if hour1 > hour2 {
                return false
            } else {
                return minute1 < minute2
            }
        })
    }
}

// MARK: - UITableViewDataSource
extension HomeController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let headerSection = sections[section]
        return headerSection.entries.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: HeaderCell.self))
        
        guard let headerCell = cell as? HeaderCell else { return cell }
        let headerSection = sections[section]
        let time = timeFormatter.string(from: headerSection.date)

        headerCell.configure(time: time)
        return headerCell
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let headerSection = sections[indexPath.section]
        
        let entry = headerSection.entries[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: HomeCell.self), for: indexPath)
        guard let homeCell = cell as? HomeCell else { return cell }
        
        homeCell.configure(entry: entry, takeTime: headerSection.date)
        
        return homeCell
    }
}

// MARK: - UITableViewDelegate

extension HomeController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = sections[indexPath.section]
        let entry = section.entries[indexPath.row]
        
        let notificationController = NotificationController(nibName: String(describing: NotificationController.self), bundle: nil)
        notificationController.entry = entry
        notificationController.takeTime = section.date
        
        notificationController.didTakeTap = { [weak self] in
            guard let self = self else { return }
            RealmManager.shared.makeDone(entry, takeTimeAt: section.date)
            self.loadData()
        }
        
        notificationController.didMissTap = { [weak self] in
            guard let self = self else { return }
            RealmManager.shared.makeUndone(entry, takeTimeAt: section.date)
            self.loadData()
        }
        notificationController.modalPresentationStyle = .overFullScreen
        present(notificationController, animated: true)
    }
}

extension HomeController {
    
    func compareTwoTimes(date1: Date, date2: Date, formatter: DateFormatter) -> Bool {
        
        let dateString1 = formatter.string(from: date1)
        let dateString2 = formatter.string(from: date2)
        
        return dateString1 == dateString2
    }
    
    func nowDateIsBetween(date1: Date, date2: Date) -> Bool {
        return (date1...date2).contains(Date())
    }
}
