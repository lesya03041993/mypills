//
//  HomePlaceholderController.swift
//  myPills
//
//  Created by Alesya Nemkova on 18.12.21.
//

import UIKit
import SwiftUI

class HomePlaceholderController: UIViewController {
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func addNewMedicine(_ sender: Any) {
        let addFirstMedication = AddFirstMedicationController(nibName: String(describing: AddFirstMedicationController.self), bundle: nil)
        let navigationController = UINavigationController(rootViewController: addFirstMedication)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
}
