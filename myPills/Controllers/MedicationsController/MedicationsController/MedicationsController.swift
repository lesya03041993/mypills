//
//  MedicationsController.swift
//  myPills
//
//  Created by Alesya Nemkova on 18.12.21.
//

import UIKit

class MedicationsController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var medicationEntery: [MedicationEntry] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        medicationEntery = RealmManager.shared.getEntries()
        tableView.reloadData()
    }
    
    private func configureUI() {
        tableView.backgroundColor = .mainAppColor
        tableView.separatorStyle = .none
        
        let medicationCell = UINib(nibName: String(describing: MedicationCell.self), bundle: nil)
        tableView.register(medicationCell, forCellReuseIdentifier: String(describing: MedicationCell.self))
    }
}

extension MedicationsController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return medicationEntery.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let entry = medicationEntery[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: MedicationCell.self), for: indexPath)
        
        guard let medicationCell = cell as? MedicationCell else { return cell }
        
        medicationCell.configure(entry: entry)
        
        return medicationCell
    }
}

extension MedicationsController: UITableViewDelegate {
     func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let entry = medicationEntery[indexPath.row]
            RealmManager.shared.deleteEntry(entry)
            medicationEntery.remove(at: indexPath.row)
            tableView.performBatchUpdates({
                tableView.deleteRows(at: [indexPath], with: .fade)
            })
        }
    }
}

