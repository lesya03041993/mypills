//
//  MedicationsContainerController.swift
//  myPills
//
//  Created by Alesya Nemkova on 9.12.21.
//

import UIKit

class MedicationsContainerController: UIViewController {

    let placeholderVC = MedicationsPlaceholderController()
    let mainVC = MedicationsController()
 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureNavBar()
        showMain()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        showPlaceholderIfNeeded()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func configureNavBar() {
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.title = "Мои лекарства"
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addMedications))
        navigationItem.rightBarButtonItem?.tintColor = .tint
        
        let appearance = UINavigationBarAppearance()
        appearance.backgroundColor = .mainAppColor
        appearance.largeTitleTextAttributes = [.foregroundColor: UIColor.tint, .font: UIFont.heavyFont(with: 34) ]
        appearance.shadowColor = .clear
        
        navigationController?.navigationBar.standardAppearance = appearance
        navigationController?.navigationBar.compactAppearance = appearance
        navigationController?.navigationBar.scrollEdgeAppearance = appearance
        
        }
    
    @objc private func addMedications() {
        let addFirstMedication = AddFirstMedicationController(nibName: String(describing: AddFirstMedicationController.self), bundle: nil)
        let navigationController = UINavigationController(rootViewController: addFirstMedication)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }

    func showMain() {
        add(mainVC)
    }
    
    func showPlaceholderIfNeeded() {
        if RealmManager.shared.getEntries().isEmpty {
            
            guard !placeholderVC.view.isDescendant(of: view) else {
                return
            }
            
            add(placeholderVC)
            
        } else {
            self.placeholderVC.willMove(toParent: nil)
            self.placeholderVC.removeFromParent()
            self.placeholderVC.view.removeFromSuperview()
        }
    }
}

