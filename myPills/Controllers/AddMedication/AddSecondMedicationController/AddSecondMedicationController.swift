//
//  AddSecondMedicationController.swift
//  myPills
//
//  Created by Alesya Nemkova on 11.01.22.
//

import UIKit

class AddSecondMedicationController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    
    var entry: MedicationEntry!

    private var times: [MedicationEntry.Reception.TakeTime] = [
        .defaultTime()
    ]
    private var startDate = Date()
    private var endDate = Date()
    private var frequency = Frequency.everyday {
        didSet {
            print()
        }
    }
    
    enum Sections: Int, CaseIterable {
        case frequency = 0
        case time
        case notifications
        
        var height: CGFloat {
            return 25
        }
        
        enum Row {
            case frequency
            case daysOfWeek
            case startEndDate
            case time
            case addTimeButton
            case notifications
            
            var height: CGFloat {
                switch self {
                case .frequency: return 150
                case .daysOfWeek: return 50
                case .startEndDate: return 150
                case .time: return 70
                case .addTimeButton: return 70
                case .notifications: return 60
                }
            }
        }
        
        var title: String {
            switch self {
            case .frequency: return "Частота"
            case .time: return "Время приема"
            case .notifications: return "Уведомления"
            }
        }
    }
    
    
    // MARK: - View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        configureNavBar()
        setupViewCollection()
    }
    
    private func rowsForSection(_ section: Sections) -> [Sections.Row] {
        switch section {
        case .frequency:
            return [.frequency, .daysOfWeek, .startEndDate]
        case .time:
            var array: [Sections.Row] = Array(repeating: .time, count: times.count)
            array.append(.addTimeButton)
            return array
        case .notifications:
            return [.notifications]
        }
    }
    
    @IBAction func saveButton(_ sender: Any) {
        entry.reception = MedicationEntry.Reception(
            frequency: frequency,
            startDate: startDate,
            endDate: endDate,
            takeTimes: times
        )

        PushManager.shared.scheduleNotifications(
            startDate: entry.reception.startDate,
            endDate: entry.reception.endDate,
            times: entry.reception.takeTimes.map { $0.time },
            medicationName: entry.name
        )
        RealmManager.shared.writeEntry(entry)
        dismiss(animated: true)
    }
    
    
    func configureNavBar() {
        navigationItem.title = "Добавить лекарство"
        
        let appearance = UINavigationBarAppearance()
        appearance.backgroundColor = .mainAppColor
        appearance.titleTextAttributes = [.foregroundColor: UIColor.tint, .font: UIFont.heavyFont(with: 24)]
        appearance.shadowColor = .clear
        
        let closeButton = UIBarButtonItem(barButtonSystemItem: .close, target: self, action: #selector(closeAddController))
        
        self.navigationItem.rightBarButtonItem = closeButton
        
        navigationController?.navigationBar.standardAppearance = appearance
        navigationController?.navigationBar.compactAppearance = appearance
        navigationController?.navigationBar.scrollEdgeAppearance = appearance
    }
    
    @objc func closeAddController() {
        dismiss(animated: true, completion: nil)
    }
    
    private func setupViewCollection() {
        let frequencyNib = UINib(nibName: String(describing: FrequencyCell.self), bundle: nil)
        collectionView.register(frequencyNib, forCellWithReuseIdentifier: String(describing: FrequencyCell.self))
        
        let daysOfWeekNib = UINib(nibName: String(describing: DaysOfWeekCollectionableCell.self), bundle: nil)
        collectionView.register(daysOfWeekNib, forCellWithReuseIdentifier: String(describing: DaysOfWeekCollectionableCell.self))
        
        let durationNib = UINib(nibName: String(describing: DurationCell.self), bundle: nil)
        collectionView.register(durationNib, forCellWithReuseIdentifier: String(describing: DurationCell.self))
        
        let titleViewNib = UINib(nibName: String(describing: TitleView.self), bundle: nil)
        collectionView.register(titleViewNib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: String(describing: TitleView.self))
        
        let addTimeNib = UINib(nibName: String(describing: AddTimeCell.self), bundle: nil)
        collectionView.register(addTimeNib, forCellWithReuseIdentifier: String(describing: AddTimeCell.self))
        
        let addTimeButtonNib = UINib(nibName: String(describing: AddTimeButtonCell.self), bundle: nil)
        collectionView.register(addTimeButtonNib, forCellWithReuseIdentifier: String(describing: AddTimeButtonCell.self))
        
        let notificationsNib = UINib(nibName: String(describing: NotificationsCell.self), bundle: nil)
        collectionView.register(notificationsNib, forCellWithReuseIdentifier: String(describing: NotificationsCell.self))
    }
}

extension AddSecondMedicationController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return Sections.allCases.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let someSection = Sections(rawValue: section) else { return 0 }
        return rowsForSection(someSection).count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let section = Sections(rawValue: indexPath.section) else {
            return UICollectionViewCell()
        }
        
        let rows = rowsForSection(section)
        
        switch section {
        case .frequency:
            let row = rows[indexPath.row]
            
            switch row {
            case .frequency:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: FrequencyCell.self), for: indexPath)
                guard let frequencyCell = cell as? FrequencyCell else { return cell }
                frequencyCell.typeChanged = { [weak self] type in
                    
                    switch type {
                    case .everyday:
                        self?.frequency = .everyday
                    case .specific:
                        self?.frequency = .specific(days: [.mon])
                    }
                    self?.collectionView.reloadData()
                }
                
                switch self.frequency {
                case .everyday:
                    frequencyCell.configure(selectedType: .everyday)
                case .specific(_):
                    frequencyCell.configure(selectedType: .specific)
                }
                return frequencyCell
            case .daysOfWeek:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: DaysOfWeekCollectionableCell.self), for: indexPath)
                guard let daysOfWeekCell = cell as? DaysOfWeekCollectionableCell else { return cell }
                
                daysOfWeekCell.daysChanged = { [weak self] days in
                    self?.frequency = .specific(days: days)
                }
                
                switch frequency {
                case .everyday:
                    daysOfWeekCell.configureCell(isEnabled: false, selectedDays: DayOfWeek.allCases)
                case .specific(let days):
                    daysOfWeekCell.configureCell(isEnabled: true, selectedDays: days)
                }
                
                return daysOfWeekCell
            case .startEndDate:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: DurationCell.self), for: indexPath)
                guard let durationCell = cell as? DurationCell else { return cell }
                
                durationCell.startDateChanged = { [weak self] startDate in
                    self?.startDate = startDate
                }
                
                durationCell.endDateChanged = { [weak self] endDate in
                    self?.endDate = endDate
                }
                
                return durationCell
            default:
                return UICollectionViewCell()
            }
        case .time:
            let row = rows[indexPath.row]
            
            switch row {
            case .time:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: AddTimeCell.self), for: indexPath)
                guard let addTimeCell = cell as? AddTimeCell else { return cell }
                
                addTimeCell.timeChanged = { selectedTime in
                    self.times[indexPath.row].time = selectedTime
                }
                
                addTimeCell.countChanged = { selectedCount in
                    self.times[indexPath.row].count = selectedCount
                }
                
                addTimeCell.removeClick = { [weak self] in
                    self?.remove(at: indexPath)
                }
                return addTimeCell
            case .addTimeButton:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: AddTimeButtonCell.self), for: indexPath)
                guard let addTimeButtonCell = cell as? AddTimeButtonCell else { return cell }
                
                addTimeButtonCell.click = { [weak self] in
                    self?.addNewTime()
                }
                return addTimeButtonCell
            default: return UICollectionViewCell()
            }
        case .notifications:
            let row = rows[indexPath.row]
            switch row {
            case .notifications:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: NotificationsCell.self), for: indexPath)
                guard let notificationsCell = cell as? NotificationsCell else { return cell }
                return notificationsCell
            default: return UICollectionViewCell()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        guard let section = Sections(rawValue: indexPath.section) else {
            return UICollectionReusableView()
        }
        
        let titleView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: String(describing: TitleView.self), for: indexPath) as! TitleView
        
        titleView.configureView(name: section.title)
        return titleView
    }
}

extension AddSecondMedicationController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        guard let section = Sections(rawValue: section) else {
            return .zero
        }
        
        let height: CGFloat
        
        switch section {
        case .notifications:
            height = 0
        default:
            height = section.height
        }
        return CGSize(width: collectionView.bounds.width , height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        guard let section = Sections(rawValue: indexPath.section) else {
            return .zero
        }
        
        let rows = rowsForSection(section)
        let currentRow = rows[indexPath.row]
        return CGSize(width: collectionView.frame.width, height: currentRow.height)
    }
}

extension AddSecondMedicationController {
    
    func addNewTime() {
        times.append(.defaultTime())
        
        let section = Sections.time
        let rows = self.rowsForSection(section)
        let newRowIndex = rows.count-2
        self.collectionView.performBatchUpdates({
            self.collectionView.insertItems(at: [IndexPath(item: newRowIndex, section: section.rawValue)])
        })
    }
    
    func remove(at indexPath: IndexPath) {
        times.remove(at: indexPath.row)
        self.collectionView.performBatchUpdates({
            self.collectionView.deleteItems(at: [indexPath])
        })
    }
}

extension MedicationEntry.Reception.TakeTime {
    static func defaultTime() -> MedicationEntry.Reception.TakeTime {
        return MedicationEntry.Reception.TakeTime(count: 0.5, time: Date.makeDate(hr: 10, min: 0), isComplete: nil)
    }
}
