//
//  AddFirstMedicationController.swift
//  myPills
//
//  Created by Alesya Nemkova on 6.01.22.
//

import UIKit

class AddFirstMedicationController: UIViewController {
    
    var entry = MedicationEntry(
        name: "",
        type: .pill,
        strengh: "",
        kind: .mg,
        color: .blueForType,
        reception: .init(frequency: .everyday, startDate: Date(), endDate: Date(),
                         takeTimes: [])
    )
    
    enum Sections: Int, CaseIterable {
        case name = 0
        case type
        case color
        case strengh
        case kind
        
        var height: CGFloat {
            switch self {
            case .name: return 140
            case .strengh: return 120
            case .type: return 170
            case .color: return 120
            case .kind: return 70
           
            }
        }
    }
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        configureNavBar()
        setupViewCollection()
    }
    
    @IBAction func nextSecondVc(_ sender: Any) {
        let addSecondMedicationController = AddSecondMedicationController(nibName: String(describing: AddSecondMedicationController.self), bundle: nil)
        addSecondMedicationController.entry = entry
        navigationController?.pushViewController(addSecondMedicationController, animated: true)
    }
    
    
    private func setupViewCollection() {
        let nameNib = UINib(nibName: String(describing: NameCell.self), bundle: nil)
        collectionView.register(nameNib, forCellWithReuseIdentifier: String(describing: NameCell.self))
        
        let typeCollectionableNib = UINib(nibName: String(describing: TypeCollectionableCell.self), bundle: nil)
        collectionView.register(typeCollectionableNib, forCellWithReuseIdentifier: String(describing: TypeCollectionableCell.self))
        
        let colorCollectionableNib = UINib(nibName: String(describing: ColorCollectionableCell.self), bundle: nil)
        collectionView.register(colorCollectionableNib, forCellWithReuseIdentifier: String(describing: ColorCollectionableCell.self))
        
        let kindCollectionableNib = UINib(nibName: String(describing: KindCollectionableCell.self), bundle: nil)
        collectionView.register(kindCollectionableNib, forCellWithReuseIdentifier: String(describing: KindCollectionableCell.self))
    }
    
    func configureNavBar() {
        navigationItem.title = "Добавить лекарство"
        
        let appearance = UINavigationBarAppearance()
        appearance.backgroundColor = .mainAppColor
        appearance.titleTextAttributes = [.foregroundColor: UIColor.tint, .font: UIFont.heavyFont(with: 24)]
        appearance.shadowColor = .clear
        
        let closeButton = UIBarButtonItem(barButtonSystemItem: .close, target: self, action: #selector(closeAddController))
        
        self.navigationItem.rightBarButtonItem = closeButton
        
        navigationController?.navigationBar.standardAppearance = appearance
        navigationController?.navigationBar.compactAppearance = appearance
        navigationController?.navigationBar.scrollEdgeAppearance = appearance
        
        }
    
    @objc func handleTap(recognizer: UITapGestureRecognizer){
        self.view.endEditing(true)
    }
    
    @objc func closeAddController() {
        dismiss(animated: true, completion: nil)
    }
}

extension AddFirstMedicationController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return Sections.allCases.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let aCase = Sections(rawValue: indexPath.section) else {
            return UICollectionViewCell()
        }
        
        switch aCase {
        case .name:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: NameCell.self), for: indexPath)
            guard let nameCell = cell as? NameCell else { return cell }
            nameCell.setupTitle(title: "Название лекарства", keyBoardNumber: false)
            
            nameCell.nameChanged = { [weak self] text in
                guard let self = self else { return }
                self.entry.name = text
            }
            
            return nameCell
      
        case .type:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: TypeCollectionableCell.self), for: indexPath)
            guard let typeCell = cell as? TypeCollectionableCell else { return cell }
            
            typeCell.typeChanged = { [weak self] aCase in
                guard let self = self else { return }
                self.entry.type = aCase
            }
            
            return typeCell
            
        case .color:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ColorCollectionableCell.self), for: indexPath)
            guard let colorCell = cell as? ColorCollectionableCell else { return cell }
            
            colorCell.colorChanged = { [weak self] color in
                guard let self = self else { return }
                self.entry.color = color
            }
            
            return colorCell
            
        case .strengh:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: NameCell.self), for: indexPath)
            guard let strenghCell = cell as? NameCell else { return cell }
            strenghCell.setupTitle(title: "Разовая доза", keyBoardNumber: true)
            
            strenghCell.nameChanged = { [weak self] text in
                guard let self = self else { return }
                self.entry.strengh = text
            }
                
            return strenghCell
            
        case .kind:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: KindCollectionableCell.self), for: indexPath)
            guard let kindCell = cell as? KindCollectionableCell else { return cell }
            
            kindCell.kindChanged = { [weak self] aCase in
                guard let self = self else { return }
                self.entry.kind = aCase
            }
            return kindCell
        }
    }
}

extension AddFirstMedicationController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        guard let aCase = Sections(rawValue: indexPath.section) else {
            return .zero
        }
        
        return CGSize(width: collectionView.frame.width, height: aCase.height)
    }
}
