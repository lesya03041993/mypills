//
//  NotificationController.swift
//  myPills
//
//  Created by Alesya Nemkova on 6.01.22.
//

import UIKit

class NotificationController: UIViewController {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var strengthLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var undoneButton: UIButton!
    
    let timeFormatter = DateFormatter()
    
    var entry: MedicationEntry?
    var takeTime: Date?
  
    var didTakeTap: (() -> ())?
    var didMissTap: (() -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        
        timeFormatter.dateFormat = "HH:mm"
        
        guard let entry = entry else {
            return
        }

        colorView.backgroundColor = entry.color.withAlphaComponent(0.2)
        imageView.image = entry.type.image
        nameLabel.text = entry.name
        strengthLabel.text = "\(entry.strengh) \(entry.kind)"
        typeLabel.text = "\(entry.type.name)"
        
        if let count = entry.reception.takeTimes.first(where: { $0.time == takeTime })?.count {
            countLabel.text = "Принять \(count)"
        }
        
        guard let takeTime = takeTime else {
            return
        }
        
        let timeString = timeFormatter.string(from: takeTime)
            timeLabel.text = "в \(timeString)"
    }
    
    func setupUI() {
        containerView.layer.cornerRadius = 10
        colorView.layer.cornerRadius = colorView.frame.height / 2
        
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(blurEffectView)
        view.bringSubviewToFront(containerView)
    }
    
    @IBAction func doneButton(_ sender: Any) {
        didTakeTap?()
        closeNotification(UIButton())
    }
    
    @IBAction func undoneButtonAction(_ sender: Any) {
        didMissTap?()
        closeNotification(UIButton())
    }
    
    @IBAction func closeNotification(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
